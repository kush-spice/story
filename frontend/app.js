var app = angular.module('myApp', ['ui.router']);
app.config(function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise("/");
  $stateProvider
    .state('login', {
      url: "/",
      templateUrl: "views/login.html",
      controller: "loginCtrl"
    })
    .state('user', {
      url: "/user",
      templateUrl: "views/user.html",
      controller: "userCtrl"
    })
    .state('edit', {
      url: "/edit",
      templateUrl: "views/edit.html",
      controller: "editCtrl"
    });
});