var app = angular.module('app', ['ui.bootstrap', 'ui.bootstrap.datetimepicker']);

app.controller('MyController', ['$scope', function($scope) {

    $scope.dates = {
        date1: null
    };

    $scope.open = {
        date1: false
    };

    $scope.openCalendar = function(e, date) {
        $scope.open[date] = true;
    };
    
    $scope.submit = function() {
      console.log("submit");
    }
    
}]);