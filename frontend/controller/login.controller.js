angular.module('myApp').controller('loginCtrl', ['$rootScope', '$scope','$http','$state',
	function ($rootScope, $scope, $http,$state) {

		console.log('login controller');
		$scope.uname="";
		$scope.pwd="";
		$scope.warning=false;
		$scope.success= false;
		// $scope.send = function(){
		// var fcm_server_key = "AIzaSyCgpTKTaOYE9E60zrLh3tRun7P0tmmUaec";

	 //    $http({
	 //      method: "POST",
	 //      dataType: 'jsonp',
	 //      headers: {'Content-Type': 'application/json', 'Authorization': 'key=' + fcm_server_key},
	 //      url: "https://fcm.googleapis.com/fcm/send",
	 //      data: JSON.stringify(
	 //          {
	 //            "notification":{
	 //              "title":"Ionic 2 FCM Starter",  //Any value
	 //              "body": 'hdfsjfhs',  //Any value
	 //              "sound": "default", //If you want notification sound
	 //              "click_action": "FCM_PLUGIN_ACTIVITY",  //Must be present for Android
	 //              "icon": "fcm_push_icon"  //White icon Android resource
	 //            },
	 //            "data":{
	 //              "param1":"value1",  //Any data to be retrieved in the notification callback
	 //              "param2": 'gsfhdfjh'
	 //            },
	 //            "to":"/topics/all", //Topic or single device
	 //            "priority":"high", //If not set, notification won't be delivered on completely closed iOS app
	 //            "restricted_package_name":"" //Optional. Set for application filtering
	 //          }
	 //        )
	 //    }).success(function(data){
	 //      $scope.reply = $scope.formData.message;
	 //      alert("Success: " + JSON.stringify(data));
	 //    }).error(function(data){
	 //      alert("Error: " + JSON.stringify(data));
	 //    });

	 //    }

	    
		$scope.login=function(){
			console.log("logging in");
			var req = {
				method: 'POST',
				url: 'http://localhost:3000/users/login',
				data: { username: $scope.uname , password : $scope.pwd }
			}
			$http(req).then(function(response){
				console.log(response.data);
				if(response.data.status=="Success"){
					$scope.success=true;
					$scope.warning=false;
					$state.go('user');
				}
				if(response.data.status=="Failed"){
					$scope.warning=true;
					$scope.success=false;
				}
			}, function(error){
				console.log(error);
			});
		}

	}]);