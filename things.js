var express = require('express');
var router = express.Router();

router.get('/',function(request,response){
	response.send('get method');
});

router.post('/',function(request, response){
	response.send('post method');
});

module.exports = router;