var MongoClient = require('mongodb').MongoClient;
var db ;
var connected = false;

module.export = {

	connect: function(url, callback){

		MongoClient.connect(url, function(err, _db){
			if (err) { throw new Error('Couldnt connect'+err); }

			db = _db;
			connected = true;

			callback(db);
		});

	},
	collection: function(name){

		if(!connected){
			throw new Error('Must Connecct To Mongo Before Calling Collection');
		}

		return db.collection(name);
	}
	
};